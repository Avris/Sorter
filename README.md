# Sorter – Lightweight sorting of tables

Just add `[data-sort]` attributes to the `th` elements in columns you'd like to sort a table by,
include ~1kB of JS & CSS, and initialise with `sorter()` – and that's it!

 - [Demo](https://sorter.avris.it)
 - [Source code](https://gitlab.com/Avris/Sorter) 
 - [NPM](https://www.npmjs.com/package/avris-sorter) 

## Installation

You can either install Sorter as a node module:

    $ npm i --save avris-sorter
    
    or
    
    $ yarn add avris-sorter

Or use a CDN:

    <link rel="stylesheet" href="https://glcdn.githack.com/Avris/Sorter/raw/v0.0.2/dist/Sorter.min.css">

    <script src="https://glcdn.githack.com/Avris/Sorter/raw/v0.0.2/dist/Sorter.min.js"></script>
    
## Usage

### HTML

Just make sure that the columns you want to be able to sort by have a `[data-sort]` attibute:

    <th data-sort>Column</th>
    
If you want to treat the values in the column as numbers, use:

    <th data-sort="number">Column</th>

Instead of using the column innerText for sorting, you can specify any arbitrary value in the `[data-sort-value]` attribute of a cell
and set `[data-sort]` of the column to `data` or `data-number`:

    <th data-sort="data-number">Balance</th>
    
    ...
    
    <td data-sort-value="38.12">38.12€</td>
    
If you want the table to be automatically sorted by some column after the page is loaded, you can set `[data-sort-auto=<direction>]`:

    <th data-sort="number" data-sort-auto="desc">Points</th>

### JS

    import sorter from 'avris-sorter';
    
    sorter()
    // or
    sorter(<selector>)
    // or
    sorter(<domElement>)

## Development

    yarn
    yarn build

## Copyright
 
 * **Author:** Andrea Vos [(avris.it)](https://avris.it)
 * **Licence:** [OQL](https://oql.avris.it/license?c=Andrea%20Vos%7Chttps%3A%2F%2Favris.t)
