var gulp = require('gulp');
var babel = require('gulp-babel');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var replace = require('gulp-replace');
var cleanCSS = require('gulp-clean-css');

gulp.task('js', function() {
    return gulp.src('src/*.js')
        .pipe(replace('module.exports = ', 'window.sorter = '))
        .pipe(babel())
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('dist'));
});

gulp.task('css', function() {
    return gulp.src('src/*.css')
        .pipe(cleanCSS())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('dist'));
});

gulp.task('default', gulp.parallel(['js', 'css']));
