const _sortTable = ($table, column, type, dir = null) => {
    const rows = $table.rows;
    const headers = rows[0].getElementsByTagName('th');

    if (dir === null) {
        dir = headers[column].dataset.sortActive === 'asc' ? 'desc' : 'asc';
    }

    const isData = type.startsWith('data');
    const isNumber = type.endsWith('number');
    const elements = [];
    for (let i = 1; i < rows.length; i++) {
        let value = rows[i].getElementsByTagName('td')[column];

        value = isData
            ? value.dataset.sortValue
            : value.innerText.trim().toLowerCase();

        if (isNumber) {
            value = parseFloat(value) || 0;
        }

        elements.push({value, row: rows[i]})
    }

    elements.sort((a, b) => {
        // reversed, because of the way we insert it in the DOM
        let r = isNumber
            ? a.value - b.value
            : ('' + a.value).localeCompare('' + b.value);

        return dir === 'desc' ? r : -r;
    });

    elements.forEach(({row}) => {
        row.parentNode.insertBefore(row, rows[0].nextSibling);
    });

    for (let i = 0; i < headers.length; i++) {
        headers[i].dataset.sortActive = undefined;
    }
    headers[column].dataset.sortActive = dir;
};

module.exports = (selector = 'table') => {
    const $tables = selector instanceof Node
        ? [selector]
        : document.querySelectorAll(selector);

    $tables.forEach($table => {
        $table.querySelectorAll('[data-sort]').forEach(($col, i) => {
            const {sort, sortAuto} = $col.dataset;
            $col.onclick = e => {
                if (event.target === $col) {
                    _sortTable($table, i, sort);
                }
            };

            if (sortAuto) {
                _sortTable($table, i, sort, sortAuto);
            }
        });
    });
}
